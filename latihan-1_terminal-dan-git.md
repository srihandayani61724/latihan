# Latihan 1 - Terminal dan GIT

Pada latihan kali ini kamu akan diajak untuk mengasah pemahaman dan kemampuan kamu dalam menggunakan tools terminal dan GIT. Kamu juga diminta untuk sedikit mengingat kembali pemahaman kamu mengenai materi sebelumnya, yaitu **Javascript**.  

Di langkah terakhir, kamu akan sedikit berkenalan dengan fitur gitlab yang dikenal dengan istilah CI/CD (Continuous integration and Continuous delivery).  

Apa saja langkah-langkah yang akan kamu kerjakan pada latihan kali ini?
1. [Membuat Private Remote Repository di Gitlab](#1-membuat-private-remote-repository-di-gitlab)
2. [Menyiapkan Working Directory GIT dengan terminal](#2-menyiapkan-working-directory-git-dengan-terminal)
3. [Menambahkan dokumen kedalam Staging Directory](#3-menambahkan-dokumen-kedalam-staging-directory)
4. [Membuat commit ke dalam Local Repository](#4-membuat-commit-ke-dalam-local-repository)
5. [Mengirimkan commit ke Remote Repository (push)](#5-mengirimkan-commit-ke-remote-repository-push)
6. [Membuat branch baru di Local Repository](#6-membuat-branch-baru-di-local-repository)
7. [Push branch baru ke Remote Repository](#7-push-branch-baru-ke-remote-repository)
8. [Merge branch yang baru kamu buat ke master/main](#8-merge-branch-yang-baru-kamu-buat-ke-mastermain)
9. [Push branch master ke remote repository](#9-push-branch-master-ke-remote-repository)
10. [Pantau CI/CD (Gitlab Pipelines)](#10-pantau-cicd-gitlab-pipelines)
11. [Hot Fix](#11-hot-fix)
12. [Menghapus branch baru](#12-menghapus-branch-baru)

## Latihan Working Directory, Staging Directory, Local Repository, Remote Repository

### 1. Membuat Private Remote Repository di Gitlab
Buatlah repository baru di [gitlab.com](https://gitlab.com) dengan ketentuan sebagai berikut:

- nama repository bebas, namun harus dibuat dalam format lowercase (huruf kecil)
- tidak ada karakter apapun selain alphanumeric kecuali symbol dash "`-`" (alfabet, nomor dan dash)
- set repository visibility menjadi Public
- buat repository tersebut kosong tanpa readme

### 2. Menyiapkan Working Directory GIT dengan terminal
Buatlah directory / folder menggunakan **Terminal** di komputer / laptop kamu, dengan ketentuan sebagai berikut:

- nama directory bebas, namun harus dibuat dalam format lowercase (huruf kecil)
- tidak ada karakter apapun selain alphanumeric kecuali symbol dash "`-`" (alfabet, nomor dan dash)
- lakukan inisialisasi git pada folder tersebut
- daftarkan remote repository gitlab yang sudah kamu buat pada langkah 1 ke folder tersebut
- buatlah satu file kosong dengan nama **README.md**
- edit file **README.md** dengan command `nano` atau `echo` dengan isi bebas, namun dengan ketentuan setidaknya ada judul (h1)
- cek status git untuk memastikan bahwa **README.md** masih dalam status untracked

<div style="page-break-after: always"></div>

### 3. Menambahkan dokumen kedalam Staging Directory
Pada tahap ini kamu telah memodifikasi isi dokumen, selanjutnya kamu harus masukkan dokumen tersebut ke staging area. cek status git untuk memastikan bahwa perubahan sudah masuk ke staging directory.

### 4. Membuat commit ke dalam Local Repository
Buatlah commit dengan pesan sesuai yang kamu inginkan. isi pesan pesan bebas dan tidak harus panjang. cek status git untuk memastikan bahwa sudah tidak ada changes / perubahan.

### 5. Mengirimkan commit ke Remote Repository (push)
Kirimkan semua perubahan ke remote repository yang sudah kamu buat pada langkah 1. Pastikan pada tahap ini semua berjalan lancar.

### 6. Membuat branch baru di Local Repository
Buatlah branch baru yang berasal dari branch utama, dengan ketentuan sebagai berikut:

- nama branch bebas, namun sebisa mungkin dibuat dalam format lowecase
- tidak ada karakter apapun selain alphanumeric kecuali symbol dash "`-`" (alfabet, nomor dan dash)
- masuk ke dalam branch tersebut
- replace file **README.md** dengan file **README.md** yang sudah diberikan
- tambahkan file **hitung.js**, **test.js** dan **.gitlab-ci.yml** ke working directory kamu
- selanjutnya kamu bisa ulangi langkah 3 dan 4

### 7. Push branch baru ke Remote Repository
Push branch baru ke remote repository, pastikan branch tersebut berhasil ditambahkan di remote repository beserta semua perubahannya.

<div style="page-break-after: always"></div>

## Latihan 2 - merge dan mengenal CI/CD

### 8. Merge branch yang baru kamu buat ke master/main
Merge branch yang baru saja kamu buat ke dalam branch master / main.

### 9. Push branch master ke remote repository
Push branch master yang telah di merge dengan branch yang baru kamu buat ke remote repository (gitlab). Pastikan semua perubahan telah ter-update di gitlab.

### 10. Pantau CI/CD (Gitlab Pipelines)
Buka respository kamu di gitlab, kemudian buka menu **CI/CD** > **Pipelines**. Pantau langkah-langkah CI/CD, jika hasilnya fail maka lanjut ke langkah ke 11 dan seterusnya.

### 11. Hot Fix
Pada langkah ini kamu perlu melakukan hotfix di repository gitlab, rinciannya sebagai berikut.

- checkout kembali ke dalam branch yang baru kamu buat tadi
- lakukan perbaikan pada file **hitung.js**.
- selanjutnya kamu bisa ulangi langkah 3, 4, 5, 8, 9 dan 10.
- jika dirasa masih belum passed, silahkan ulangi langkah 11

### 12. Menghapus branch baru
Jika langkah 11 sudah selesai hapus branch baru baik di local maupun di remote repository.

## SELAMAT MENCOBA
> Kamu di persilahkan latihan terlebih dahulu.  
> Jika dirasa sudah siap dan paham semua langkah diatas  
> kamu bisa mengajukan diri untuk present atau  
> menunggu untuk diminta mempersentasikannya.